<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author  : Midhun Raj R S
 * Descr : Controller handels all actions from Subject Expert
 * Date : 7/7/2021
 */
class SEaction extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->database();
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->data = array(
            'template' => 'common/template',
            'controller' => $this->router->fetch_class(),
            'method' => $this->router->fetch_method(),
            'session_userdata' => isset($this->session->userdata) ? $this->session->userdata : '',
            'base_url' => base_url(),
            'site_url' => site_url(),
            
        );
    }
    public function index(){
        $data = array('title' => 'SERequirementList', 'page' => 'homepage', 'errorCls' => NULL, 'post' => $this->input->post());
        $data = $data + $this->data;
        $this->load->view('includes/header', $data);
        $this->load->view('includes/sidebar', $data);
        $this->load->view('SEmodule/listExam', $data);
        $this->load->view('includes/footer', $data);
    }
    
    public function questionPaperPreperation(){
        $data = array('title' => 'questionPreperation', 'page' => 'PreperationView', 'errorCls' => NULL, 'post' => $this->input->post());
        $data = $data + $this->data;
        
        // Remove this Static values //
        $_SESSION['sel_category'] = ''; // Remove static value
        $_SESSION['hid_qstno'] = ''; // Remove static value
        $_SESSION['hid_qst'] = ''; // Remove static value
        $_SESSION['user_id'] = ''; // Remove static value
        
        //Static Values Ends Here //

        if (isset($_SESSION['sel_category'])){
            unset($_SESSION['sel_category']);
	}
	
	if (isset($_SESSION['hid_qstno'])){
            unset($_SESSION['hid_qstno']);
	}
	
	if (isset($_SESSION['hid_qst'])){
            unset($_SESSION['hid_qst']);
	}
        
        $userid = trim($_SESSION['user_id']);
        
        
        
        
        $this->load->view('includes/header', $data);
        $this->load->view('includes/sidebar', $data);
        $this->load->view('SEmodule/questionPreperationView', $data);
        $this->load->view('includes/footer', $data);
    }
}
