<style>
    .request-overlay {
        z-index: 9999;
        position: fixed; /*Important to cover the screen in case of scolling content*/
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        display: block;
        text-align: center;
        background: rgba(200,200,200,0.5) url('../../../assets/images/loading/loading2.gif') no-repeat center; /*.gif file or just div with message etc. however you like*/
    }
</style>
<div class="request-overlay" style="display: none;"></div>

