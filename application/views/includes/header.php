<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <style>.form-inline { display: block; }</style>
        <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.custom.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/fullcalendar.min.css'); ?>" />
        <!-- text fonts -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/fonts.googleapis.com.css'); ?>" />
        <!-- ace styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css'); ?>" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="<?php echo base_url('assets/custom/cstyle.css'); ?>" />
        <script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.dataTables.bootstrap.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/dataTables.buttons.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/buttons.flash.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/buttons.html5.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/buttons.print.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/buttons.colVis.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/dataTables.select.min.js'); ?>"></script>

        <!-- ace scripts -->


        <script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fullcalendar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootbox.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.custom.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.ui.touch-punch.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.easypiechart.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.sparkline.index.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.flot.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.flot.pie.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.flot.resize.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/ace-elements.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/ace.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>

        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='<?php echo base_url('assets/js/jquery.mobile.custom.min.js'); ?>'>" + "<" + "/script>");
        </script>


        <script src="<?php echo base_url('assets/js/ace-elements.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/ace.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/datepicker/new/moment-2.10.3.js'); ?>"></script> 
        <script src="<?php echo base_url('assets/datepicker/new/bootstrap-datetimepicker.js'); ?>"></script>


        <!-- for report getorgchart -->
        <script src="<?php echo base_url('assets/plugins/getorgchart/getorgchart.js'); ?>"></script>


        <script type="text/javascript">
            var BASE_URL = "<?php echo base_url(); ?>";
            var base_url = "<?php echo base_url(); ?>";
        </script>
        <script type="text/javascript">
            jQuery(function ($) {
                //initiate dataTables plugin
                var myTable =
                        $('#dynamic-table')
                        .DataTable({
                            bAutoWidth: false,
                            "aaSorting": [],
                            select: {
                                style: 'multi'
                            }
                        });
            });
        </script>	
        <!-- PAGE CONTENT ENDS -->
        <script  type="text/javascript">
            $(document).ready(function ()
            {

            });
        </script>
        <script type="text/javascript">
            try {
                ace.settings.loadState('main-container')
            } catch (e) {
            }
        </script>
        <script>
            var csfrData = {};
            csfrData['<?php echo $this->security->get_csrf_token_name(); ?>']
                    = '<?php echo $this->security->get_csrf_hash(); ?>';

            $(function () {
                // Attach csfr data token
                $.ajaxSetup({
                    data: csfrData
                });
            });
        </script>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>SE Module</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>">
        <meta name="theme-color" content="#103175">
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <!-- bootstrap & fontawesome -->

        <script src="<?php echo base_url('assets/js/ace-extra.min.js'); ?>"></script>

        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.custom.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/fullcalendar.min.css'); ?>" />
        <!-- text fonts -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/fonts.googleapis.com.css'); ?>" />
        <!-- ace styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css'); ?>" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace-skins.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace-rtl.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ace-rtl.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jquery.confirm/jquery.confirm.css'); ?>" />


        <link rel="stylesheet" href="<?php echo base_url('assets/datepicker/new/style.css'); ?>" />

        <link href="<?php echo base_url('assets/plugins/getorgchart/getorgchart.css'); ?>" rel="stylesheet" />

        <script src="<?php echo base_url('assets/plugins/jquery.confirm/jquery.confirm.js'); ?>"></script>

    </head>
    <div class=" affix" style="position: fixed;  top: 0;  left: 0;  width: 100%; z-index:99999; background: #3b5998">
        <div class="col-md-7"><img src="<?php echo base_url('assets/images/logo.png'); ?>" class="img-responsive"></div>

        <div class="col-md-5 " style="text-align: right">
            <ul class="nav nav-tabs " style="text-align: right; border-bottom:0px">
                <li>
                    <?php
                    $intPaswdChangeStatus = $this->session->userdata('intPaswdChangeStatus');
                    $editprofile = 0;
                    if ($this->session->userdata('intCampStaffID')) {
                        if ($intPaswdChangeStatus == 1) {
                            $editprofile = 1;
                        }
                    } else {
                        $editprofile = 1;
                    }



                    if (($editprofile == 1)) {
                        ?>				
                        <a class="btn btn-success" href="<?php echo base_url(); ?>index.php/Login/ProfileEdit">
                            <i class="ace-icon fa fa-user"></i>
                        <?php echo $this->session->userdata('vchrName') . "(" . $this->session->userdata('vchrUserName') . ")"; ?>
                        </a>
<?php } ?>
                </li>
                <li><?php
                    if ($this->session->userdata('intCampStaffID')) {
                        if ($intPaswdChangeStatus == 1) {
                            ?>	


                            <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/Login/CampStaffPasswordChange">
        <?php
    }
} else {
    ?>
                            <a class="btn btn-warning" href="<?php echo base_url(); ?>index.php/Login/PasswordChange">
                        <?php } ?>

                            <i class="ace-icon fa fa-cog"></i>
                            Change Password
                        </a><?php // }  ?>
                </li>


                <li>
                    <a class="btn btn-danger btn-xs" href="<?php echo base_url(); ?>index.php/Login/logout">
                        <i class="ace-icon fa fa-power-off"></i>
                        Logout
                    </a>
                </li>
            </ul></div>
    </div>
    <body class="no-skin" style="background:#d8dfea ">



        <div class="main-container ace-save-state" id="main-container">
            <div class="main-content">
                <div class="main-content-inner">
                    <div style="margin-top: 60px; width:100%">

                    </div>