<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>OSMS | Evaluator Login</title>
				<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>">
				<meta name="theme-color" content="#103175">

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap4.1.3.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts.googleapis.com.css'); ?>" />

		<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-rtl.min.css'); ?>" />
	</head>

		<!-- /.main-container -->
		
		<script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/bootstrap4.1.3.bundle.min.js'); ?>"></script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});		
			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('#btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'light-blue');
				
				e.preventDefault();
			 });
			 
			});
		</script>

	
	
<style>
.login-block{
    
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url('assets/images/pexels-photo.jpg'); ?>)  no-repeat left bottom; background-size:; min-height:50px; border-radius: 0 10px 10px 0; padding:0;}
.containerw{background:#fff; border-radius: 10px; box-shadow:15px 20px 0px rgba(0,0,0,0.1);}
.carousel-inner{border-radius:0 10px 10px 0;}
.carousel-caption{text-align:left; left:5%;}
.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec .copy-text i{color:#3b5998;}
.login-sec .copy-text a{color:#3b5998;}
.login-sec h3{margin-bottom:30px; margin-top: 30px; font-weight:800; font-size:20px; color: #3b5998;}
.login-sec h3:after{content:" "; width:80%; height:5px; background:#ffc107; display:block; margin-top:10px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #3b5998; color:#fff; font-weight:600;}
.banner-text{width:100%; position:absolute; bottom:40px; padding-left:20px; text-align:justify}
.banner-text h2{color:#ffc107; font-weight:600;}
.banner-text h3:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
.banner-text p{color:#fff;}
	.badge {
    display: inline-block;    padding: .25em .4em;font-size: 50%;  font-weight: 700;  line-height: 0.75;
    text-align: center;    white-space: nowrap;    vertical-align: baseline;    border-radius: .25rem;
}
</style>	
                     <?php
										$attributes = array("class" => "form-horizontal", "id" => "myformlogin", "name" => "myformlogin", "method"=>"post" );
    									echo form_open_multipart("Login/campStaffLogin_check", $attributes);?>

	<body style="background: #3b5998;  background: -webkit-linear-gradient(to bottom, #3b5998, #d8dfea); 
background: linear-gradient(to bottom, #3b5998, #d8dfea); ">
  <section class="login-block">
   <div class="container" align="center" style=" margin-bottom: 30px; color: white; font-family:  Impact, Haettenschweiler, Franklin Gothic Bold, Arial Black,' sans-serif'"><img src="<?php echo base_url('assets/images/logo.png'); ?>" class="img-responsive"><h1>ON-SCREEN MARKING SYSTEM <small style="font-size: 12" class="badge badge-warning"> 1.0</small></h1></div>
    <div class="container containerw">
		
	<div class="row">
		<div class="col-md-4 login-sec" >
	    <h3 class=" center" style="background: #3b5998"><small style="color: #fff"><?php
echo "&nbsp; &nbsp; Server time: " . date("d-M-y, D h:i:s A");
?></small></h3>
		    <h3 class="text-center">EVALUATOR LOGIN</h3>
		    <form class="login-form">
  <div class="form-group">
    <label for="exampleInputEmail1" class="text-uppercase">Username</label>
    	<input name="txt_usrid" required id="txt_usrid" type="text" class="form-control" placeholder="Username" />
   <!-- <input type="text" class="form-control" placeholder="">-->
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="text-uppercase">Password</label>
    <!--<input type="password" class="form-control" placeholder="">-->
    	<input name="pass_usrpswd" required id="pass_usrpswd" type="password" class="form-control" placeholder="Password" />
  </div>
  
  
    <div class="form-check">
    <label class="form-check-label">
      
      <?php 
																echo $this->session->flashdata('msg_valid');
														?>
    </label>
    <button type="submit" name="campStaffLogin" id="campStaffLogin"  class="btn btn-login float-right">Submit</button>
  </div>
  
</form>
<div class="copy-text"><i class="fa fa-user"></i> For Administrator Login <a href="<?php echo base_url('index.php/Login/'); ?>" />Click here </a></div>
		</div>
		<div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

                  </ol>
            <div class="carousel-inner" role="listbox">

    <div class="carousel-item active">
      <img class="d-block img-fluid" src="<?php echo base_url('assets/images/pexels-photo.jpg'); ?>" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2>Online Evaluation</h2>
            <p>Provides an extensive platform for evaluating pen and paper based summative examinations on computer screens. On-Screen Marking system completely automates the marking and scoring of summative/descriptive answer scripts into an accurate, simple, secure and efficient process.</p>
        </div>	
    </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="<?php echo base_url('assets/images/pexels-photo.jpg'); ?>" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2>Evaluation monitoring</h2>
            <p>Administrator in charge can configure complete examination related activities through this module Subjects, question papers and marking schemes are managed in a paperless environment. </p>
        </div>	
    </div>
    </div>
            </div>	   
		    
		</div>
	</div>
</div>
	  </div>
	  <div class="container" align="center" style="margin-top: 30px;  color: #3b5998; font-family:  Haettenschweiler, Franklin Gothic Bold, Arial Black,' sans-serif'"> &copy; 2017-<?php echo date("Y");?> | Designed and Developed by Centre for Development of Imaging Technology <a class="text-primary" target="_blank" href="http://cdit.org" title="Clicking will take yout to the External website of C-DIT in new window">(C-DIT)</a>, Under Govt. of Kerala</div>
</section>
	</body>
</html>