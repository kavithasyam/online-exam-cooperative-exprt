/*
 * File : Common script file for camp functions
 * @author : Jyothi
*/

function Assignqus(config){

    var $doc = $(document);
    
	var config = (typeof(config) == 'undefined') ? {} : config;
	if(config.init == 1)
	{
		init();
	}
     
     function init()
	{
		  $doc.ready(function(){
         
         initSelect2();
         showCenters();
         showEvaluators();
         showCamps();
         addQuestionRow();
         removeQuestionRow();
         activateEndQuestion();
         showDeleteConfirm();
         onReserveCheck();
         showSelectedLevel();
	 checkAll();


     
         

     $.validator.addMethod("alpha", function(value, element) {
          return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
      },"Only Characters Allowed.");

      $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9 &\-)( ]+$/i.test(value);
      }, "Only Characters and Numbers Allowed");

      $.validator.addMethod("mobileno", function(value,element) {
                    return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
      }, "Unsupported Format");

  });
			
		
	}

function showSubmitBtn(){
 
  var reserved = $('.reserved');
  var tables   = $('.assign_table');
  if(reserved.length == tables.length){
    $('.btn_submit').prop('disabled',true);
  }else{
    $('.btn_submit').prop('disabled',false);

  }

}


function onReserveCheck(){

  $doc.on('change','.reservechk',function(){

    var $this = $(this);
    var row = $this.parent().parent();
    var table =  row.find('.assign_table');
    var inputs = row.find('.edit_in');
     if(this.checked){
		 row.find(".selqus").val("");
		 row.find('.rangebox').prop('checked', false);
         inputs.prop("disabled", true);
         table.removeClass('unreserved');
         $this.addClass('reserved');

         showSubmitBtn();
       
     }else{
       // endqus.val("");
        inputs.prop("disabled", false);
        table.addClass('unreserved');
        $this.removeClass('reserved');

        showSubmitBtn();
     }

  });
}

  
function initSelect2(){
  
  
  $(".cl_select2").select2({
   // tags: true,
  });
}

function showCamps(){

  $doc.on('change','#exam',function(){
 $('#center').val('');
	  $('#subpage').html('');
     var examID = $(this).val();
      if(examID !=""){
     $.ajax({
        url:base_url+"index.php/AssignQuestion/getExamCampsAjax/",
        type:'POST',
        data: {exid : examID },
        dataType : 'html',
        success:function(response){
            
             $('#camp').html(response);
            if(response.status == "success"){
                details = response.details;
                 
            }
        }
      });
}
  });
}

function showCenters(){

  $doc.on('change','#camp',function(){
     var campID = $(this).val();
     $.ajax({
        url:base_url+"index.php/AssignQuestion/getExamCentersAjax/",
        type:'POST',
        data: {caid : campID },
        dataType : 'html',
        success:function(response){
            
             $('#center').html(response);
            if(response.status == "success"){
                details = response.details;
                 
            }
        }
      });
  });
}


function showEvaluators(){

  $doc.on('change','#center',function(){
     var cid = $(this).val();
      var examID = $('#exam').val();
       $('#subpage').html('');
      if(cid.length !=0){
     $.ajax({
        url:base_url+"index.php/AssignQuestion/getEvaluatorsAjax/",
        type:'POST',
        data: {cid:cid,exid:examID},
        dataType : 'html',
        success:function(response){
            $('#subpage').html(response);
            initSelect2();
            showSelectedLevel();
checkAll();

            validateSelectedQuestions();
           // addQuestionRow();
          //  removeQuestionRow();
           // activateEndQuestion();
          //  checkEvaluators();
        }
      });
 }
  });
}

function validateSelectedQuestions(){
	

  $doc.on('change','.endqus',function(){
	 
    var $this = $(this);
    var startqusel =  $this.closest('tr').find('.star_tqus');
    var startqus   = startqusel.val();
    var endqus     = $this.val();

    if(startqus.length > 0){

       var actualValues = [];    
       $this.children('option').map(function(l, e){
         actualValues.push(parseInt(e.value));    
       }); 
       actualValues.shift(); 

       var selectedRange = [];
       for (var i = startqus; i <= endqus; i++) {
         selectedRange.push(parseInt(i));
      }
   
     diff = $(selectedRange).not(actualValues).get();
	 console.log($(this).closest('tr').find('.errSpan'));
	 $(this).closest('tr').find('.errSpan').remove();
     if(diff.length > 0){
         $this.after("<span class='errSpan'><font color='red'>Invalid Range selected </font></span>");
          $this.val("");
     }else{
       $('.errSpan').remove();
     }
     /*var difference = [];

    $.grep(selectedRange, function(el) {
        if ($.inArray(el, actualValues) == -1) difference.push(el);
    });*/
   


    }else{
      $this.val("");
    }
   
  });
}



function addQuestionRow(){
  
  $doc.on('click','.add_btn',function(){

    var $this = $(this);
    var rowToAdd = $this.closest('tr').clone();
    rowToAdd.find(".selqus").val("");
    rowToAdd.find(".rangebox").prop('checked', false);
    rowToAdd.find(".endqus").prop("disabled", true);
	rowToAdd.find(".del_btn").show();
    $this.hide();
    $this.next().show();
    var table = $this.closest('table');
console.log(rowToAdd);//$("#product_table tbody tr:first").clone();
            table.append(rowToAdd);
            $('.errSpan').remove();
    
  });

}

function removeQuestionRow(){

  $doc.on('click','.del_btn',function(){
    var $this = $(this);
    var table = $this.closest('table');
    var trcount = $('tr',table.find('tbody')).length;
    if(trcount >1){
      $this.closest('tr').remove();
      trcount = $('tr',table.find('tbody')).length;
      if(trcount ==1){
        table.find('.del_btn').hide();
      }
	  table.find('tr:last .add_btn').show();
    }else{
       $this.hide()
       $this.prev().show();
    }
   //$("#product_table tbody tr:first").clone();
            
    
  });
}

function activateEndQuestion(){
  
  $doc.on('change','.rangebox',function(){
    var $this = $(this);
    var endqus =  $this.closest('tr').find('.endqus');
     var err_span = $this.closest('tr').find('.sp_error'); 
     if(this.checked){
        endqus.prop("disabled", false);
        //validateSelectedQuestions();
     }else{
        endqus.val("");
        endqus.prop("disabled", true);
        err_span.html('');

     }
     
     
  });

}


  
  function showDeleteConfirm(){

    $doc.on('click','.del_evalqus',function(evt){
      
       evt.preventDefault();
       var id = $(this).data('id');
        $.confirm({
          'title'   : 'Delete Confirmation',
          'message' : 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
          'buttons' : {
          'Yes' : {
            'class' : 'blue',
            'action': function(){
                window.location.href = base_url+"index.php/AssignQuestion/deleteEvaluatorAssignQuestions/"+id;
              }
          },
          'No'  : {
            'class' : 'gray',
            'action': function(){
               return false;
              }  // Nothing to do in this case. You can as well omit the action property.
            }
          }
        });
    });
  }

  /*****************************************************************************************************************/

   function showSelectedLevel(){

    $('.check_level').on('change',function(){
         if(this.checked){
          $(this).addClass('levelSelected');
          $('.check_level').not(this).prop('checked', false);
          $('.check_level').not(this).removeClass('levelSelected');
          var $level = this.value;
           $('.assignTypeDiv').hide();
          if($level == 'question'){
            
             $('.evaluatorsDiv').show();
           
          }else if($level == 'script'){
	         $('.evaluatorsListDiv').show();
          }else if($level == 'subject'){
           $('.evaluatorsListDivForSubject').show();
          }
      

        }
    });
  }
  
  function checkAll(){
	$('.checkall').on('change',function(){
		 if(this.checked){
			 $('.assCheck').prop('checked', true);
		 }else{
			 $('.assCheck').prop('checked', false);
		 }
	});
       $('.assCheck').on('change',function(){
          if(!this.checked){
		$('.checkall').prop('checked', false);
	  }
       });
     
  }
}
if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Assignqus(config);
