/*
 * File : Common script file for monitoring camp process 
 * @author : Jyothi
*/

function Monitoring(config){

    var $doc = $(document);
    var jcrop_api ="";
	var config = (typeof(config) == 'undefined') ? {} : config;
    var myTable;
	if(config.init == 1)
	{
		init();
	}

	function init(){

		$doc.ready(function(){
           OnChangeQuestion();
           onChangeNewEvaluator();
           initDataTable();
           showChildTable();
           hideChildTable();

          showCEChildTable();


		});
	}

    



	function initDataTable(){
	   myTable = $('#monitoring-table').DataTable( {
							bAutoWidth: false,
							"aaSorting": [],
							select: {
								style: 'multi'
							}	
			    } );
	}
function showCEChildTable(){
        $doc.on('click','.showCEChild',function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = myTable.row(tr);
            var evaluator     = tr.find(".evaluator").val();
            var panel         = tr.find(".panel").val();
            var campID        = $(".campId").val();
            var examID        = $(".examId").val();
            

            $.ajax({
                url:base_url+"index.php/Campmonitoring/getCheifEvaluationData/",
                type:'POST',
                data: { evaluator,campID,examID,panel },
                dataType : 'html',
                success:function(response){
                   row.child(response).show();
                   tr.find('.showCEChild').hide();
                   tr.find('.hideChild').show();
                } 
            });
           
        });
    }

function showChildTable(){
        $doc.on('click','.showChild',function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = myTable.row(tr);
            var evaluator     = tr.find(".evaluator").val();
            var panel         = tr.find(".panel").val();
            var campID        = $(".campId").val();
            var examID        = $(".examId").val();
            

            $.ajax({
                url:base_url+"index.php/Campmonitoring/getEvaluationData/",
                type:'POST',
                data: { evaluator,campID,examID,panel },
                dataType : 'html',
                success:function(response){
                   row.child(response).show();
                   tr.find('.showChild').hide();
                   tr.find('.hideChild').show();
				   
                } 
            });
           
        });
    }

function hideChildTable(){

      $doc.on('click','.hideChild',function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = myTable.row(tr);
             row.child.hide();
             tr.find('.showChild').show();
             tr.find('.hideChild').hide();
			  tr.find('.showCEChild').show();
           
        });
}




	/* Functions on re assigning */
     
     function OnChangeQuestion(){
//alert("gg");
		$doc.on('change','#question_no',function(){

			var question_no  = $("#question_no").val();
            var currentStaff = $("#assignedStaff").val();
            var campDate     = $("#campDate").val();  
            var intSubjectID    =$("#intSubjectID").val();
            var level = $('#level').val();             
			if(question_no.length > 0){
			 $.ajax({
        		url:base_url+"index.php/Campmonitoring/getUnAssignedEvaluators/",
        		type:'POST',
                //data: {question_no,currentStaff,campDate,level,level},

        		data: {question_no,currentStaff,campDate,level,level,intSubjectID},
        		dataType : 'json',
        		success:function(response){
                    
        			//table-div
             		$('#newevaluator').html();
             		$('#newevaluator').html(response.evaluators);
                $('#scriptCount').val(response.unattendedScriptsCount);
                $('#qusmark').val(response.mark);
                $('#totalmark').val(parseInt(response.mark)*parseInt(response.unattendedScriptsCount));
                onChangeReassignNumber();
                if(response.msg != "" ){
                        $('.warning_msg').html(response.msg);
                        $('.div_warning').show();
                }

             		
        		},
                    error:function(response)
            {
                alert('error');
            }
      		});//ajax
			}

		});
	}

    function onChangeNewEvaluator(){

        $doc.on('change','#newevaluator',function(){

            var newevaluator  = $("#newevaluator").val();
            var question_no  = $("#question_no").val();
            $('.div_warning').hide();
            $('.warning_msg').html("");

            if(newevaluator.length > 0){
             $.ajax({
                url:base_url+"index.php/Campmonitoring/getNewEvaluatorBalanceLimit/",
                type:'POST',
                data: {newevaluator,question_no},
                dataType : 'json',
                success:function(response){
                  if(response.assigned > 0){
                   
                    var msg = "Already assigned "+response.assigned+" question/s for the selected evaluator";
                    $('.warning_msg').html(msg);
                     $('.div_warning').show();
                  }


                    
                }
            });//ajax
            }

        });
    }

    function onChangeReassignNumber(){

       $doc.on('change','#reassignNo',function(){
            $("#erid").html('');
            var scriptCount = $("#scriptCount").val();
            var reassignNo  = $("#reassignNo").val();
            var mark        = $("#qusmark").val();
            if(reassignNo <= scriptCount){
               
               $('#reassignmark').val(parseInt(reassignNo)*parseInt(mark));
            }else{
               $("#reassignNo").val('');
               $("#erid").html('Invalid Number');
               
            }

           

        });

    }


}

if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Monitoring(config);
              
