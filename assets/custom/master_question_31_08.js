/*
 * File : Common script file for master questions functions
 * @author : Jyothi
 
*/

function Master_question(config){

    var $doc = $(document);
    var jcrop_api ="";
	var config = (typeof(config) == 'undefined') ? {} : config;
	if(config.init == 1)
	{
		init();
	}
     
     function init()
	{
		$doc.ready(function(){
         
         getExamCategory();
         previewMasterQustn();
         uploadFormValidate();

         //template marking
         initJcrop();
         showNextPage();
         showPrevPage();
         submitCordinates();
         getSpans();
        // validateQuestion();
         switchAddBtns();
         checkMark();

         // edit template
         editCordinates();
         switchEditBtns();

			


     /* $.validator.addMethod("alpha", function(value, element) {
          return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
      },"Only Characters Allowed.");

      $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9 &\-)( ]+$/i.test(value);
      }, "Only Characters and Numbers Allowed");

      $.validator.addMethod("mobileno", function(value,element) {
                    return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
      }, "Unsupported Format");*/
			
		});
	}
  //

 
  function checkMark(){
    $doc.on('change','#max_mark',function(){

      var $this = $(this);
      var mark = $(this).val();
      var balance = $('#tobe_add').val();
      if(mark.length != 0){
         var examId = $('#examId').val();
         var tempId = $('#templateId').val();
           $.ajax({

                 url: base_url+"index.php/Scan/checkExamMark", 
                 async: false,
                 type: "POST", 
                 dataType: "json",
                 cache: false, 
                 data:{mark:mark,examId:examId,tempId:tempId,balance:balance},
                 success: function(response) {
                     if(response.status == 'error'){
                         swal(
                            'error!',
                            response.msg,
                             'error'
                          );
                       $this.val('');
                    }
                  }
           });       
      }
    });

  }

  function switchEditBtns(){
     
     $doc.on('change','#edit_answer_span',function(){


       var span = $(this).val();
       if(span == "E" || span == "A"){
        $('#submit_btn').hide();
        $('#confirm_btn').show();
       }else{
          $('#submit_btn').show();
        $('#confirm_btn').hide();
       }
     });
  }

  function editCordinates(){
     
           $('#edit_marking_form').validate({
      rules : {
        
        answer_span : { required : true},
        max_mark : { required : true},
        subject  : { required : true},

      },
      messages : {
        
        answer_span : { required : "Required"},
        max_mark  : { required : "Required"},
        subject   : { required : "Required"}
      },
      submitHandler: function(form) {
       
        w = $('#w').val();
       
        if(w.length <= 0){
           
           swal(
                  'Error!',
                  'Select the area for marking',
                  'error'
                );
           return false;
         }else {

         $.ajax({
                    url: base_url+"index.php/Scan/editCordinates", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:$('#edit_marking_form').serialize(),
                    success: function(response) { 
                        if(response.status=="success")
                        {  
                           swal(
                            'Success!',
                            'Area of question added successfully!',
                             'success'
                          );
                        
                         if(response.qus_stat == "marked"){
                               
                               if(response.confirm =="yes"){
                                window.location.href = base_url+'index.php/Scan/MarkingTemplates';
                               }
                              
                              
                          }else{
                            $('#edit_answer_span').val("");
                            $('#edit_answer_span').html(response.select);
                            $('option', '#subject').not(':eq(0), :selected').prop("disabled", true);

                              var btncl = 'btn_'+response.qus_no;
                              $('.'+btncl).prop('disabled', false);
                              jcrop_api.destroy();
                              initJcrop();
                          }
                          //
                         // window.location.href = "<?php echo site_url("UserPdfUpload/croppedImage"); ?>";
                          
                        }
                    }
            });
       }
      }
    });
    
  }


   function getSpans(){

      $doc.on('change','#qustion_no',function(){

          $("#max_mark").val("");
          $("#max_mark").prop("readonly", false);
          var qus_no = $('#qustion_no').val();
          var template = $('#templateId').val();
          $.ajax({
              url: base_url+"index.php/Scan/getAnswerSpans", 
              async: false,
              type: "POST", 
              dataType: "json",
              cache: false, 
              data:{ 
                qus_no :qus_no,
                template:template
              },
              success: function(response) { 
                      $('#answer_span').html(response.select);
                      if(response.mark.length != 0){
                        $("#max_mark").val(response.mark);
                        $("#max_mark").prop("readonly", true);
                        $('#subject').val(response.subject);
                        $('option', '#subject').not(':eq(0), :selected').prop("disabled", true);
                      }
              }
            });
      });
  
    }
 function validateQuestion(){

      $doc.on('change','#answer_span,#qustion_no',function(){

        var qus_no = $('#qustion_no').val();
        var span = $('#answer_span').val();
        var template = $('#templateId').val();
        if(qus_no != "" && span != ""){

           $.ajax({
                    url: base_url+"index.php/Scan/validateQuestion", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:{ qus_no :qus_no,
                           span:span,
                           template:template
                         },
                    success: function(response){
                        if(response.status == 'error')
                        {
                          $('#qustion_no').val("");
                          $('#answer_span').val("");
                        }
                    }
            });
        }

      });
    }
  
   function switchAddBtns(){
     
    $doc.on('change','#answer_span',function(){
      var span = $(this).val();

      if($('#tobe_add').val() == 1 || $('#tobe_add').val() == 0){
        if(span == "E" || span == "A"){
          $('#submit_btn').hide();
          $('#confirm_btn').show();
        }else{
          $('#submit_btn').show();
          $('#confirm_btn').hide();
        }
      }
    });
  }

  function submitCordinates(){
     
           $('#template_marking_form').validate({
            ignore: "",
      rules : {
        qustion_no : { required : true },
        answer_span : { required : true},
        max_mark : { required : true},
        subject : { required : true},
      //  w:{ required : true},
       


      },
      messages : {
        qustion_no : { required : "<font color='red'>Required</font>"},
        answer_span : { required : "<font color='red'>Required</font>"},
        max_mark  : { required : "<font color='red'>Required</font>"},
        subject   : { required : "<font color='red'>Required</font>"},
      //  w : { required : "<font color='red'>Required Area</font>"},
       
      },
      submitHandler: function(form) {
        
        w = $('#w').val();
       
        if(w.length <= 0){
           
           swal(
                  'Error!',
                  'Select the area for marking',
                               'error'
                          );
           return false;
         }else {

            $.ajax({
                    url: base_url+"index.php/Scan/addCordinates", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:$('#template_marking_form').serialize(),
                    success: function(response) { 
                        if(response.status=="success")
                        {  
                          swal(
                            'Success!',
                            'Area of question added successfully!',
                             'success'
                          );

                          $('#answer_span').val("");
                          $('#qustion_no').val("");
                          $('#max_mark').val("");
                          $('.area_inputs').val('');
                          $('#subject').val('');


                         jcrop_api.destroy();
                          initJcrop();
                          if(response.qus_stat == "marked"){
                               if(response.confirm =="yes"){
                                window.location.href = base_url+'index.php/Scan/MarkingTemplates';
                               }
                              
                            
                              $("#qustion_no option[value='"+response.qus_no+"']").remove();
                             
                             var count = $('#tobe_add').val();
                             $('#tobe_add').val(count-1);

                          }else{
                            
                            //$('#max_mark').prop('disabled', true);
                          }

                           var btncl = 'btn_'+response.qus_no;
                           $('.'+btncl).show();
                           $('.'+btncl).prop('disabled', false);
                          
                        }
                    }
            });

        }
      
       
      }
    });
    
  }

  /*
  * functions to show th next page
  *
  */
   function showNextPage(){
       $doc.on('click','#next_image',function(){
          $('#div_page').html("");
          var next_val = $("#img_no" ).val();
          var next_val = Number(next_val)+1;
          if(next_val >= images.length){
               next_val = images.length-1;
           }
           $('#target').attr( 'src' , filepath+images[next_val]);
              $("#img_no" ).val(next_val);
                $element = "<img src="+filepath+images[next_val]+"  id='target' alt=''/>";

                $('#div_page').html($element);
                initJcrop();
       });
       
   }
   
    function showPrevPage(){
       $doc.on('click','#prev_image',function(){
          $('#div_page').html("");
          var prev_val = $("#img_no" ).val();
          var prev_val = Number(prev_val)-1;
          if(prev_val<= 0)
            {
              prev_val = 0;
            }
         
           $('#target').attr( 'src' , filepath+images[prev_val]);
              $("#img_no" ).val(prev_val);
                $element = "<img src="+filepath+images[prev_val]+" width='80%' id='target' alt=''/>";

                $('#div_page').html($element);
                initJcrop();
       });
       
   }
  /*
  * function to initialize jcrop
  * @date 27/04/2018
  */
  function initJcrop(){

     $('#target').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      onRelease:  clearCoords,
      addClass: 'jcrop-centered'
    },function(){
      jcrop_api = this;
    });
   
  }

  function showCoords(c)
  {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function clearCoords()
  {
    $('#coords input').val('');
  }
  /*
  * function to show the exam category on selecting the exam
  * ajax request
  * @date 24/04/2018
  */
  function getExamCategory(){
    
    $doc.on('change',"#sel_exam",function(){
 
          var val = $(this).val();
      $.ajax({
                    url:'get_exam_details',
                    type:'POST',
                    data: {examId : val },
                    dataType : 'json',
                    success:function(response){

                      if(response.status == "success"){
                        details = response.details;
                          if(details.vchrExamCode!=""){
                             $('#ExamDetailTable').show();
                          
                          var val = "<strong>"+details.vchrExamCode+"</strong>"
                          $('#category_td').html(val);
                          }
                         
                      }

                  }
              });
    });
  }
  /*
  * Function to show the preview of uploaded pdf file
  * @date : 23/04/2018
  */
    function previewMasterQustn(){

      $doc.on('change','#master_file',function(){
          var control = $('#master_file');
          var  filename = $('#master_file').val().replace(/C:\\fakepath\\/i, '');
        // alert(filename);return false;
        var valid_extensions = /(\.pdf|\.PDF)$/i;   
        var allowedtype = $('#allowedtype').val();
        var allowedsize = $('#allowedsize').val();
        var fileExtension = filename.substr(filename.indexOf('.',2));
        fileExtension = fileExtension.replace('.','');
        var valid_extensions = [allowedtype,allowedtype.toUpperCase()];
        
      if(!$.inArray(fileExtension,valid_extensions)){ 
        var uploadSize = document.getElementById("master_file").files[0].size;
        if(Math.round(uploadSize*0.000001) <= allowedsize){
            pdffile=document.getElementById("master_file").files[0];
            pdffile_url=URL.createObjectURL(pdffile);
            $('#viewer').attr('src',pdffile_url);
            $('#viewer').show();
        }else{
            //$('#viewer').hide();
                      document.getElementById('master_file').value = ''

                swal(
                            'Error!',
                            'File Size Exceeds !!!',
                             'error'
                          );

                return false;
        }
      }else{
        document.getElementById('master_file').value = ''
          swal(
                'Error!',
                'Invalid File ! Please select a PDF file to upload',
                'error'
              );
         return false;
       
      }
      /* if(valid_extensions.test(filename)){ 
        pdffile=document.getElementById("master_file").files[0];
        pdffile_url=URL.createObjectURL(pdffile);
        $('#viewer').attr('src',pdffile_url);
        }else{
         alert('Invalid File ! Please select a PDF file to upload');return false;
        }*/
      });

    }

    function uploadFormValidate(){
          
          $('#master_question_form').validate({
      rules : {
        exam : {
          required : true,
          
        },
        master_file : {
          required : true,
          
        },

      },
      messages : {
        exam : {
          required : "<font color='red'>Required</font>",
        },
         master_file : {
          required : "<font color='red'>Required</font>",
        } 
        
      },
      submitHandler: function(form) {
         $('#master_question_form').submit();
        }
    });

    }
}
if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Master_question(config);