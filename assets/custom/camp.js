/*
 * File : Common script file for camp functions
 * @author : Jyothi
*/

function Camp(config){

    var $doc = $(document);
    
	var config = (typeof(config) == 'undefined') ? {} : config;
	if(config.init == 1)
	{
		init();
	}
     
     function init()
	{
		$doc.ready(function(){
         
         //showCenterForm();
          getExamChiefStatus();
           showDeleteConfirm();
          showCenter();
          initSelect2();
          CampSubmit();
          showexamDetails();
         // disableCaOfficer();
        //  disableSelectedCa($('.cl_caofficer'));

			


     $.validator.addMethod("alpha", function(value, element) {
          return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
      },"Only Characters Allowed.");

      $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9 &\-)(/ ]+$/i.test(value);
      }, "Only Characters and Numbers Allowed");

      $.validator.addMethod("mobileno", function(value,element) {
                    return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
      }, "Unsupported Format");
			
		});
	}
  //
function showexamDetails(){

   $doc.on('click','#examDtBtn',function(){
      var exam = $('#exam').val();
      $('#examDetailsDiv').html('');
        $.ajax({

                 url: base_url+"index.php/camp/getExamDetailsAjax", 
                 async: false,
                 type: "POST", 
                 dataType: "html",
                 cache: false, 
                 data:{ exam:exam },
                 success: function(response) {
                   $('#examDetailsDiv').html(response);
                    $('#examDetailModal').modal('show');
                  }
           });
       
   });

}
function initSelect2(){
  
  
  $(".cl_select2").select2({
    tags: true,
  });
}
 function showCenter(){
  $('.center_check').change(function(){
    var $this = $(this);
    var rel_box = $this.parent().parent().parent().parent().find('.rel-box');
    if(this.checked){
      rel_box.show();
      initSelect2();
      disableSelectedCa($('.cl_caofficer'));
    }else{
      rel_box.hide();
    }
   
  });
 }
  /* function to initiate  the date mask */ 

  function initDateMask(){
    
    $('.date-picker').mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
  
  }

  function CampSubmit(){
 
           $('#camp_form').validate({
      rules : {
        
           camp_name : { 
          required : true,
          alphanumeric : true,
        }
      },
      messages : {
        
        camp_name    : { required : "<font color='red'>Required</font>"},
        exam         : { required : "<font color='red'>Required</font>"},
      },
      submitHandler: function(form) {
		 
       var haserror = 0;
       var loop_chk = $('.center_check:checkbox:checked');
      
        var flag = 1;
       loop_chk.each(function (ix, element) {
          rel_box = $(element).parent().parent().parent().parent().find('.rel-box');
       // var msheet_td   =  rel_box.find('.cl_msheet');
        var maxmark_td  =  rel_box.find('.cl_maxmark');
        var sdate_td    =  rel_box.find('.cl_sdate');
        var edate_td    =  rel_box.find('.cl_endate');
        var officer_td  =  rel_box.find('.cl_caofficer');
      /*  var shour       =  rel_box.find('.cl_shour');
        var sminute     =  rel_box.find('.cl_sminute');
        var ehour       =  rel_box.find('.cl_ehour');
        var eminute     =  rel_box.find('.cl_eminute');*/
        var evaluator   =  rel_box.find('.cl_evalutor');
      
        /*if(msheet_td.val().length === 0 ){
          haserror = 1;
          msheet_td.addClass('errorcl');
          msheet_td.next('.sp_error').html('Required');
        }else{
          msheet_td.removeClass('errorcl');
        }//
        if($.isNumeric(msheet_td.val()) ){
          msheet_td.removeClass('errorcl');
        }else{
          haserror = 1;
          msheet_td.addClass('errorcl');
          msheet_td.next('.sp_error').html('Required');
        }*/
        if(maxmark_td.val().length === 0 ){
          haserror = 1;
          maxmark_td.addClass('errorcl');
          maxmark_td.next('.sp_error').html('Required');
        }else{
          maxmark_td.removeClass('errorcl');
        }//
       /* if($.isNumeric(msheet_td.val()) ){
          maxmark_td.removeClass('errorcl');
        }else{
          haserror = 1;
          maxmark_td.addClass('errorcl');
          maxmark_td.next('.sp_error').html('Required');
        }*/
        if(sdate_td.val().length === 0){
          haserror = 1;
          sdate_td.addClass('errorcl');
          sdate_td.next('.sp_error').html('Required');
        }else{
             var isValid = isValidDate(sdate_td.val());
             if (isValid) {
               sdate_td.removeClass('errorcl');
             }else {
                haserror = 1;
                sdate_td.addClass('errorcl');
                sdate_td.next('.sp_error').html('Enter valid date');
            }    
        }
        if(edate_td.val().length === 0){
          haserror = 1;
          edate_td.addClass('errorcl');
          edate_td.next('.sp_error').html('Required');
        }else{
            var isValid = isValidDate(edate_td.val());
            if (isValid) {
                if(compareDate(sdate_td.val(),edate_td.val())){
                   edate_td.removeClass('errorcl');
                    edate_td.next('.sp_error').html('');
                }else {
                   haserror = 1;
                 edate_td.addClass('errorcl');
                 edate_td.next('.sp_error').html('End date should be greater than start date');

               }
               
             }else {
                 haserror = 1;
                edate_td.addClass('errorcl');
                edate_td.next('.sp_error').html('Enter valid date');
            }
        }
         if(officer_td.val() == ""){
          haserror = 1;
          officer_td.addClass('errorcl');
          officer_td.next('.sp_error').html('Required');
        }else{
          officer_td.removeClass('errorcl');
        }
      /*  if(shour.val().length === 0){
          haserror = 1;
          shour.addClass('errorcl');
          shour.next('.sp_error').html('Required');
        }else{
          shour.removeClass('errorcl');
        }
       */
    /*  if(ehour.val().length === 0){
          haserror = 1;
          ehour.addClass('errorcl');
          ehour.next('.sp_error').html('Required');
        }else{
         var start = parseInt(shour.val()) + '.' +  parseInt(sminute.val());
         var end   = parseInt(ehour.val()) + '.' +  parseInt(eminute.val());
         if(validateTime(start,end)){
           shour.removeClass('errorcl');
           ehour.removeClass('errorcl');
           eminute.next('.sp_error').html('');
         }else{
                    haserror = 1;
           shour.addClass('errorcl');
           ehour.addClass('errorcl');
           eminute.next('.sp_error').html('End time should greater than start time');
         }
         
         
        }*/

      if( evaluator.val() != null){
           evaluator.removeClass('errorcl');
         }else{
          
             haserror = 1;
             evaluator.addClass('errorcl');
             var sel2box =rel_box.find('.select2-selection')
             sel2box.addClass('errorcl');
             sel2box.parent().parent().next('.sp_error').html('Required');
           
        }
         
           
     });
    
         
       if(haserror != 0){
          return false;
        }else{

         $(".cl_caofficer option").prop("disabled", false); 
         return true;
       }
      }
 
    });
    
  }




  function validateTime(start,end){

    startTime = parseFloat(start); 
    endTime   = parseFloat(end); 
    if(endTime > startTime ){
      return true;
    }else{
      return false;
    }
  }


/*function to validate date in format dd/mm/yyyy*/
  function isValidDate(input) {
  var regexes = [
    /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/,
    /^(\d{1,2})\-(\d{1,2})\-(\d{4})$/
  ];

  for (var i = 0; i < regexes.length; i++) {
    var r = regexes[i];
    if(!r.test(input)) {
      continue;
    }
    var a = input.match(r), d = new Date(a[3],a[2] - 1,a[1]);
    if(d.getFullYear() != a[3] || d.getMonth() + 1 != a[2] || d.getDate() != a[1]) {
      continue;
    }
    // All checks passed:
    return true;
  }

  return false;
}
/* Function to compare to date*/

 function compareDate(startDate,endDate){
   
   var sdate = convertDateFormat(startDate);
   var edate = convertDateFormat(endDate);
   if ((Date.parse(sdate) > Date.parse(edate))) {
        return false;
    }else{
       return true;
    }   
 }
 function  convertDateFormat(date){

     var d=new Date(date.split("/").reverse().join("-"));
     var dd=d.getDate();
     var mm=d.getMonth()+1;
     var yy=d.getFullYear();
     var newdate =mm+"/"+dd+"/"+yy;
     return newdate;
 }
  
  function showDeleteConfirm(){

    $doc.on('click','.del_camp',function(evt){
       evt.preventDefault();
       var id = $(this).data('id');
        $.confirm({
          'title'   : 'Delete Confirmation',
          'message' : 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
          'buttons' : {
          'Yes' : {
            'class' : 'blue',
            'action': function(){
                window.location.href = base_url+"index.php/Camp/deleteCamp/"+id;
              }
          },
          'No'  : {
            'class' : 'gray',
            'action': function(){
               return false;
              }  // Nothing to do in this case. You can as well omit the action property.
            }
          }
        });
    });
  }

  function disableCaOfficer(){
    
    $doc.on('change','.cl_caofficer',function(){
      var $this = $(this);
      var selValue = $this.val();
      var $select = $('.cl_caofficer');
      if($this.val() != ""){
          var selected = disableSelectedCa($select,$this);  
       }        
      });

   }

   function disableSelectedCa($select,$this){
      var selected = getSelectedOptions($select,$this);
      $(".cl_caofficer option").not($this).prop("disabled", false); 
      for (var index in selected) { 
        $('.cl_caofficer option[value="'+selected[index]+'"]').prop("disabled", true);
      }
   }

   function getSelectedOptions($select){

         var selected = [];  
         $.each($select, function(index, select) {           
           if (select.value !== "") { selected.push(select.value); }

         });   
         return selected;
   }

   function getExamChiefStatus(){

     $doc.on('change','#exam',function(){

      var $this = $(this);
      var exam = $this.val();
      $('#sheetcount').val('');
      if(exam != ""){

           $.ajax({

                 url: base_url+"index.php/camp/getExamChiefStatusAjax", 
                 async: false,
                 type: "POST", 
                 dataType: "html",
                 cache: false, 
                 data:{ exam:exam },
                 success: function(response) {

                     $('#formPageDiv').html(response);
                  
                     $('#examDtBtn').show();
                     initDateMask();
       
                       CampSubmit();
                       showDeleteConfirm();
                       showCenter();
                       initSelect2();
                      // disableCaOfficer();
                       //disableSelectedCa($('.cl_caofficer'));
                       CampWithoutCheifSubmit();
                       $('#camp_name').val($.trim($("#exam option:selected").text()));
                  }
           });
      }

     });


   }

       function CampWithoutCheifSubmit(){
        
           $('#camp_formwithoutCheif').validate({
      rules : {
        
           camp_name : { 
          required : true,
          alphanumeric : true,
        },
        'chk_center[]' : {
          required : true,
          minlength: 1 
        }
      },
      messages : {
        
        camp_name    : { required : "<font color='red'>Required</font>"},
        'chk_center[]'    : { required : "<font color='red'>Required</font>"},
      },
      submitHandler: function(form) {
		 
       var haserror = 0;
       var loop_chk = $('.center_check:checkbox:checked');
      
        var flag = 1;
       loop_chk.each(function (ix, element) {
          rel_box = $(element).parent().parent().parent().parent().find('.rel-box');
        //var msheet_td   =  rel_box.find('.cl_msheet');
        var maxmark_td   =  rel_box.find('.cl_maxmark');
        var sdate_td    =  rel_box.find('.cl_sdate');
        var edate_td    =  rel_box.find('.cl_endate');
        var officer_td  =  rel_box.find('.cl_caofficer');
        var shour       =  rel_box.find('.cl_shour');
        var sminute     =  rel_box.find('.cl_sminute');
        var ehour       =  rel_box.find('.cl_ehour');
        var eminute     =  rel_box.find('.cl_eminute');
        var evaluator   =  rel_box.find('.cl_evalutor');
      
     
        if(maxmark_td.val() == ""){
          haserror = 1;
          maxmark_td.addClass('errorcl');
          maxmark_td.next('.sp_error').html('Required');
        }else{
          maxmark_td.removeClass('errorcl');
        }//
        if($.isNumeric(maxmark_td.val()) ){
          maxmark_td.removeClass('errorcl');
        }else{
          haserror = 1;
          maxmark_td.addClass('errorcl');
          maxmark_td.next('.sp_error').html('Required');
        }
        if(sdate_td.val().length === 0){
          haserror = 1;
          sdate_td.addClass('errorcl');
          sdate_td.next('.sp_error').html('Required');
        }else{
             var isValid = isValidDate(sdate_td.val());
             if (isValid) {
               sdate_td.removeClass('errorcl');
             }else {
                haserror = 1;
                sdate_td.addClass('errorcl');
                sdate_td.next('.sp_error').html('Enter valid date');
            }    
        }
        if(edate_td.val().length === 0){
          haserror = 1;
          edate_td.addClass('errorcl');
          edate_td.next('.sp_error').html('Required');
        }else{
            var isValid = isValidDate(edate_td.val());
            if (isValid) {
                if(compareDate(sdate_td.val(),edate_td.val())){
                   edate_td.removeClass('errorcl');
                    edate_td.next('.sp_error').html('');
                }else {
                   haserror = 1;
                 edate_td.addClass('errorcl');
                 edate_td.next('.sp_error').html('End date should be greater than start date');

               }
               
             }else {
                 haserror = 1;
                edate_td.addClass('errorcl');
                edate_td.next('.sp_error').html('Enter valid date');
            }
        }
         if(officer_td.val() == ""){
          haserror = 1;
          officer_td.addClass('errorcl');
          officer_td.next('.sp_error').html('Required');
        }else{
          officer_td.removeClass('errorcl');
        }
       /* if(shour.val().length === 0){
          haserror = 1;
          shour.addClass('errorcl');
          shour.next('.sp_error').html('Required');
        }else{
          shour.removeClass('errorcl');
        }*/
       
   /*   if(ehour.val().length === 0){
          haserror = 1;
          ehour.addClass('errorcl');
          ehour.next('.sp_error').html('Required');
        }else{
         var start = parseInt(shour.val()) + '.' +  parseInt(sminute.val());
         var end   = parseInt(ehour.val()) + '.' +  parseInt(eminute.val());
         if(validateTime(start,end)){
           shour.removeClass('errorcl');
           ehour.removeClass('errorcl');
           eminute.next('.sp_error').html('');
         }else{
                    haserror = 1;
           shour.addClass('errorcl');
           ehour.addClass('errorcl');
           eminute.next('.sp_error').html('End time should greater than start time');
         }
         
         
        }*/

         
           
     });
    
         
       if(haserror != 0){
          return false;
        }else{

         $(".cl_caofficer option").prop("disabled", false); 
         return true;
       }
      }
 
    });
    
  }

}
if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Camp(config);