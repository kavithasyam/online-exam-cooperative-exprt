/*
 * File : Common script file for master questions functions
 * @author : Jyothi
 
*/

function Master_question(config){

    var $doc = $(document);
    var jcrop_api ="";
    var rotatearray = {};
	var config = (typeof(config) == 'undefined') ? {} : config;
	if(config.init == 1)
	{
		init();
	}
     
     function init()
	{
		$doc.ready(function(){
         
         getExamCategory();
         previewMasterQustn();
         uploadFormValidate();

         //template marking
          initJcrop();
         showNextPage();
         showPrevPage();
         submitCordinates();
         getSpans();
         
        // resizeDiv();
        // validateQuestion();
         switchAddBtns();
         checkMark();

         // edit template
         editCordinates();
         switchEditBtns();
         paginateBtns();

		 

			


     /* $.validator.addMethod("alpha", function(value, element) {
          return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
      },"Only Characters Allowed.");*/

      $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9 )( ]+$/i.test(value);
      }, "Only Characters and Numbers Allowed");

      /* $.validator.addMethod("mobileno", function(value,element) {
                    return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
      }, "Unsupported Format");*/
			
		});
	}
  //

 /* ....................functions in marking starts........................*/

 function paginateBtns(){

  $doc.on('click','.cl_paginateBtn',function(){
          $('#div_page').html("");
            next_val = $(this).val();
           $('#target').attr( 'src' , filepath+images[next_val]);
              $("#img_no" ).val(next_val);
                $element = "<img src="+filepath+images[next_val]+" id='target'  alt=''/>";

                $('#div_page').html($element);
                initJcrop();
      //resizeDiv();  
         //pageno
            var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);
            $('.cl_paginateBtn').removeClass('active');
            $('.pgbtn_'+img_no).addClass('active');

       });

 }
  function checkMark(){
    $doc.on('change','#max_mark',function(){

      var $this = $(this);
      var mark = $(this).val();
      var balance = $('#tobe_add').val();
      if(mark.length != 0){
         var examId = $('#examId').val();
         var tempId = $('#templateId').val();
           $.ajax({

                 url: base_url+"index.php/Scan/checkExamMark", 
                 async: false,
                 type: "POST", 
                 dataType: "json",
                 cache: false, 
                 data:{mark:mark,examId:examId,tempId:tempId,balance:balance},
                 success: function(response) {
                     if(response.status == 'error'){
                         swal(
                            'error!',
                            response.msg,
                             'error'
                          );
                       $this.val('');
                    }
                  }
           });       
      }
    });

  }

  function switchEditBtns(){
     
     $doc.on('change','#edit_answer_span',function(){


       var span = $(this).val();
       if(span == "E" || span == "A"){
        $('#submit_btn').hide();
        $('#confirm_btn').show();
       }else{
          $('#submit_btn').show();
        $('#confirm_btn').hide();
       }
     });
  }

  function editCordinates(){
     
           $('#edit_marking_form').validate({
      rules : {
        
        answer_span : { required : true},
        max_mark : { required : true},
        subject  : { required : true},
        qus_part : {
          alphanumeric : true,
          required : true
        }

      },
      messages : {
        
        answer_span : { required : "<font color='red'>Required</font>"},
        max_mark  : { required : "<font color='red'>Required</font>"},
        subject   : { required : "<font color='red'>Required</font>"},
        qus_part : {
          alphanumeric : "<font color='red'>Only allows A-Z,a-z,0-9,(,)</font>",
          required : "<font color='red'>Required</font>"
        }
      },
      submitHandler: function(form) {
       
        w = $('#w').val();
       
        if(w.length <= 0){
           
           swal(
                  'Error!',
                  'Select the area for marking',
                  'error'
                );
           return false;
         }else {

         $.ajax({
                    url: base_url+"index.php/Scan/editCordinates", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:$('#edit_marking_form').serialize(),
                    success: function(response) { 
                        if(response.status=="success")
                        {  
                           swal(
                            'Success!',
                            'Area of question added successfully!',
                             'success'
                          );
                        
                         if(response.qus_stat == "marked"){
                               
                               if(response.confirm =="yes"){
                               // window.location.href = base_url+'index.php/Scan/MarkingTemplates';
							   window.location.href = base_url+response.backtourl;
                               }
                              
                              
                          }else{
                            $('#edit_answer_span').val("");
                            $('#edit_answer_span').html(response.select);
                            $('option', '#subject').not(':eq(0), :selected').prop("disabled", true);

                              var btncl = 'btn_'+response.qus_no;
                              $('.'+btncl).prop('disabled', false);
                              jcrop_api.destroy();
                              initJcrop();
                          }
                          //
                         // window.location.href = "<?php echo site_url("UserPdfUpload/croppedImage"); ?>";
                          
                        }
                    }
            });
       }
      }
    });
    
  }


   function getSpans(){

      $doc.on('change','#qustion_no',function(){

          $("#max_mark").val("");
          $("#max_mark").prop("readonly", false);
          if($('#commonSubject').val() !=1){
              $('#subject').val('');
          }
          var qus_no = $('#qustion_no').val();
          var template = $('#templateId').val();
          $('#qus_part').val(qus_no);
          $.ajax({
              url: base_url+"index.php/Scan/getAnswerSpans", 
              async: false,
              type: "POST", 
              dataType: "json",
              cache: false, 
              data:{ 
                qus_no :qus_no,
                template:template
              },
              success: function(response) { 
                      $('#answer_span').html(response.select);
                      if(response.mark.length != 0){
                        $("#max_mark").val(response.mark);
                        $("#max_mark").prop("readonly", true);
                        $('#subject').val(response.subject);
                        $('option', '#subject').not(':eq(0), :selected').prop("disabled", true);
                      }else{

                         $('option', '#subject').prop("disabled", false);
                      }
              }
            });
      });
  
    }
 function validateQuestion(){

      $doc.on('change','#answer_span,#qustion_no',function(){

        var qus_no = $('#qustion_no').val();
        var span = $('#answer_span').val();
        var template = $('#templateId').val();
        if(qus_no != "" && span != ""){

           $.ajax({
                    url: base_url+"index.php/Scan/validateQuestion", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:{ qus_no :qus_no,
                           span:span,
                           template:template
                         },
                    success: function(response){
                        if(response.status == 'error')
                        {
                          $('#qustion_no').val("");
                          $('#answer_span').val("");
                        }
                    }
            });
        }

      });
    }
  
   function switchAddBtns(){
     
    $doc.on('change','#answer_span',function(){
      var span = $(this).val();

      if($('#tobe_add').val() == 1 || $('#tobe_add').val() == 0){
        if(span == "E" || span == "A"){
          $('#submit_btn').hide();
          $('#confirm_btn').show();
        }else{
          $('#submit_btn').show();
          $('#confirm_btn').hide();
        }
      }
    });
  }

  function submitCordinates(){
     
           $('#template_marking_form').validate({
            ignore: "",
      rules : {
        qustion_no : { required : true },
        answer_span : { required : true},
        max_mark : { required : true},
        subject : { required : true},
        qus_part : {
          alphanumeric : true,
          required : true
        }
      //  w:{ required : true},
       


      },
      messages : {
        qustion_no : { required : "<font color='red'>Required</font>"},
        answer_span : { required : "<font color='red'>Required</font>"},
        max_mark  : { required : "<font color='red'>Required</font>"},
        subject   : { required : "<font color='red'>Required</font>"},
        qus_part : {
          alphanumeric : "<font color='red'>Only allows A-Z,a-z,0-9,(,)</font>",
          required : "<font color='red'>Required</font>"
        }
      //  w : { required : "<font color='red'>Required Area</font>"},
       
      },
      submitHandler: function(form) {
        
        w = $('#w').val();
       
        if(w.length <= 0){
           
           swal(
                  'Error!',
                  'Select the area for marking',
                               'error'
                          );
           return false;
         }else {

            $.ajax({
                    url: base_url+"index.php/Scan/addCordinates", 
                    async: false,
                    type: "POST", 
                    dataType: "json",
                    cache: false, 
                    data:$('#template_marking_form').serialize(),
                    success: function(response) { 
                      //alert(response.status);
                        if(response.status=="success")
                        {  
                          swal(
                            'Success!',
                            'Area of question added successfully!',
                             'success'
                          );

                          $('#answer_span').val("");
                          $('#qustion_no').val("");
                          $('#max_mark').val("");
                          $('#qus_part').val("");
                          $('.area_inputs').val('');
                          if($('#commonSubject').val() !=1){
                            $('#subject').val('');
                          }
                          


                         jcrop_api.destroy();
                         initJcrop();
                          if(response.qus_stat == "marked"){
                               if(response.confirm =="yes"){
                                window.location.href = base_url+'index.php/Scan/MarkingTemplates';
                               }
                              
                            
                              $("#qustion_no option[value='"+response.qus_no+"']").remove();
                             
                             var count = $('#tobe_add').val();
                             $('#tobe_add').val(count-1);

                          }else{
                            
                            //$('#max_mark').prop('disabled', true);
                          }

                           var btncl = 'btn_'+response.qus_no;
                           $('.'+btncl).show();
                           $('.'+btncl).prop('disabled', false);
                          
                        }
                        else
                        {
                          alert("Error!  Please recheck the data");
                        }
                    }
            });

        }
      
       
      }
    });
    
  }

  function resizeDiv(){
    
     $('#zm_div').css('zoom','80%');
$('#zm_div').css('zoom','0.8%');
$('#zm_div').css('-moz-transform','scale(0.8,0.8)');
  $('.imageAppendTag').css('zoom','80%');
$('.imageAppendTag').css('zoom','0.8%');
$('.imageAppendTag').css('-moz-transform','scale(0.8,0.8)');

  }

  /*
  * functions to show th next page
  *
  */
   function showNextPage(){
       $doc.on('click','#next_image',function(){
          $('#div_page').html("");
          var next_val = $("#img_no" ).val();
          var next_val = Number(next_val)+1;
          if(next_val >= images.length){
               next_val = images.length-1;
           }
           $('#target').attr( 'src' , filepath+images[next_val]);
              $("#img_no" ).val(next_val);
                $element = "<img src="+filepath+images[next_val]+" id='target'  alt=''/>";

                $('#div_page').html($element);
                initJcrop();
			//resizeDiv();	
				 //pageno
            var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);
             $('.cl_paginateBtn').removeClass('active');
            $('.pgbtn_'+img_no).addClass('active');
       });
       
   }
   
    function showPrevPage(){
       $doc.on('click','#prev_image',function(){
          $('#div_page').html("");
          var prev_val = $("#img_no" ).val();
          var prev_val = Number(prev_val)-1;
          if(prev_val<= 0)
            {
              prev_val = 0;
            }
         
           $('#target').attr( 'src' , filepath+images[prev_val]);
              $("#img_no" ).val(prev_val);
                $element = "<img src="+filepath+images[prev_val]+" id='target'  alt=''/>";

                $('#div_page').html($element);
                initJcrop();
		//resizeDiv();		
				 //pageno
            var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);
             $('.cl_paginateBtn').removeClass('active');
            $('.pgbtn_'+img_no).addClass('active');
       });
       
   }
  /*
  * function to initialize jcrop
  * @date 27/04/2018
  */
  function initJcrop(){
    
     $('#target').Jcrop({
      onChange:   showCoords,
      onSelect:   showCoords,
      onRelease:  clearCoords,
      addClass: 'jcrop-centered',
	  keySupport: false
    },function(){
      jcrop_api = this;
    });
   
  }

  function showCoords(c)
  {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function clearCoords()
  {
    $('#coords input').val('');
  }


  /*------------------functions ends for marking-------------------------------------*/

  /*--------------------Functions for master templates starts-----------------------------------*/
  /*
  * function to show the exam category on selecting the exam
  * ajax request
  * @date 24/04/2018
  * @modified date 01/09/2018
  */
  function getExamCategory(){
    
    $doc.on('change',"#sel_exam",function(){
 
          var val = $(this).val();
          var token = $("input[name=secure_web_token]").val();
           $('.error_div').html("");

          if(val.length > 0){
      $.ajax({
                    url:'checkTemplateExist',
                    type:'POST',
                    data: {examId : val,secure_web_token:token },
                    dataType : 'json',
                    success:function(response){

                      if(response.status == "success"){
                        details = response.details;
                        var fileStatus = response.fileStatus;
                          if(details.vchrExamCode!=""){
                              var val = details.vchrExamCode;
                              $('#category_td').val(val);

                              if(fileStatus.subFile == "error"){

                                 swal(
                                    'Error!',
                                    'Images of Pages doesnot exists in Master Template Folder',
                                    'error'
                                 );
                                 $('#viewer').attr('src',"");
                                 $('#viewer').hide();
                                $('#ExamDetailTable').hide();
                                 var error = '<div class="alert alert-danger" ><strong>Error!</strong> <span class="errors_msg">Images of Pages doesnot exists in Master Template Sub Folder</span></div>';
                                    $('.error_div').append(error);
                              }else if(fileStatus.subFile == "success"){
                                   // swal("Please verify the uploaded Main PDF and Its Images");
                                var submitStatus = 1;
                                $('.error_div').html("");
                                $('#ExamDetailTable').show();
                                ShowSubImages(fileStatus.subFilePath,fileStatus.subImages,submitStatus);
                              //  $('.btn_vrImages').click();
                              rotateImages();
                              }
                          }
                         
                      }//success

                  }
              }); //ajax
        }
    });
  }

  function showMainPdf(pdffile_url){

    $doc.on('click','.btn_vrPdf',function(){
      $('.pager').hide();
	  $('.cl_pagebar').hide();
       filepath = pdffile_url
            $('#viewer').attr('src',filepath);
            $('#viewer').show();
            $('.check_vrPdf').attr('disabled',false);
    });
  }

   function ShowSubImages(subFilePath,subImages,submitStatus){

   // $doc.on('click','.btn_vrImages',function(){
       $('.pager').show();
       filepath = subFilePath+subImages[0];
            $('#viewer').attr('src',filepath);
            $('#viewer').show();
            $('.check_vrImages').attr('disabled',false);
            $('#img_no').val('0');
              var $degree =  '0deg';
			 //alert($degree);
			 //pagebar 
        $('#viewer').css({
                 "-webkit-transform": "rotate("+$degree+")",
                 "-moz-transform": "rotate("+$degree+")",
                 "transform": "rotate("+$degree+")",/* For modern browsers(CSS3)  */
                });
            var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);

            var imagesCount = subImages.length;
            $('.cl_page_count').html(imagesCount);
            $('.cl_pagebar').show();
			
            ShowNextImage(subFilePath,subImages);
            ShowPrevImage(subFilePath,subImages);
            if(submitStatus==1){
               $('.btn_submit').attr('disabled',false);

            }
            $('#total_pages').val(subImages.length);

   // });
  }

  function ShowNextImage(subFilePath,subImages){
       
        $doc.on('click','#next_timage',function(){
		
          var imgNo = $('#img_no').val();
          var imagesCount = subImages.length;
          if(imgNo >= 0 && imgNo < imagesCount-1){

            var newVal = parseInt(imgNo)+1;
            filepath = subFilePath+subImages[newVal];
            $('#viewer').attr('src',filepath);
            $('#img_no').val(newVal);
			
			      var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);
             var $degree =  '0deg';
            if (newVal in rotatearray){
               $degree = rotatearray[newVal];
                
            }
          //   alert($degree);
             $('#viewer').css({
                 "-webkit-transform": "rotate("+$degree+")",
                 "-moz-transform": "rotate("+$degree+")",
                 "transform": "rotate("+$degree+")",/* For modern browsers(CSS3)  */
                });
            
          }else{
            alert('page end');
          }
        });
  }

   function ShowPrevImage(subFilePath,subImages){
       
        $doc.on('click','#prev_timage',function(){
          var imgNo = $('#img_no').val();
          var imagesCount = subImages.length;
          if(imgNo > 0 && imgNo <= imagesCount-1){

            var newVal = parseInt(imgNo)-1;
            filepath = subFilePath+subImages[newVal];
            $('#viewer').attr('src',filepath);
            $('#img_no').val(newVal);
			      var img_no = $('#img_no').val();
            var page_no = parseInt(img_no)+1;
            $('.cl_page_no').html(page_no);
             var $degree =  '0deg';
            if (newVal in rotatearray){
                 $degree = rotatearray[newVal];
            }
        //    alert($degree);
            $('#viewer').css({
                 "-webkit-transform": "rotate("+$degree+")",
                 "-moz-transform": "rotate("+$degree+")",
                 "transform": "rotate("+$degree+")",/* For modern browsers(CSS3)  */
                });

          }else{
            alert('page end');
          }
        });
  }

  /*function to rotate the images
   * @date 29/10/2019
  */

   function rotateImages(){

    $doc.on('click','#rotate',function(){
          var imgNo = $('#img_no').val();
          var examId = $('#sel_exam').val();
          var page_no = parseInt(imgNo)+1;
           $.ajax({
              url:'rotateImage',
              type:'POST',
              data: {examId,page_no},
              dataType : 'json',
              success:function(response){

                      if(response.status == "success"){
                        $nxtdeg =  '180deg';
                          if (imgNo in rotatearray){
                            var degree = rotatearray[imgNo];
                            if(degree == '180deg'){
                              $nxtdeg = '360deg';
                            }else{
                               $nxtdeg = '180deg';
                            }
                          }
                          rotatearray[imgNo] = $nxtdeg;
                         $('#viewer').css({
                           "-webkit-transform": "rotate("+$nxtdeg+")",
                           "-moz-transform": "rotate("+$nxtdeg+")",
                          "transform": "rotate("+$nxtdeg+")",/* For modern browsers(CSS3)  */
                        });
                      }//success

                  }
              });

          
    });

  }

  /*
  * Function to show the preview of uploaded pdf file
  * @date : 23/04/2018
  */
    function previewMasterQustn(){

      $doc.on('change','#master_file',function(){
          var control = $('#master_file');
          var  filename = $('#master_file').val().replace(/C:\\fakepath\\/i, '');
        // alert(filename);return false;
        var valid_extensions = /(\.pdf|\.PDF)$/i;   
        var allowedtype = $('#allowedtype').val();
        var allowedsize = $('#allowedsize').val();
        var fileExtension = filename.substr(filename.indexOf('.',2));
        fileExtension = fileExtension.replace('.','');
        var valid_extensions = [allowedtype,allowedtype.toUpperCase()];
        
      if(!$.inArray(fileExtension,valid_extensions)){ 
        var uploadSize = document.getElementById("master_file").files[0].size;
        if(Math.round(uploadSize*0.000001) <= allowedsize){
            pdffile=document.getElementById("master_file").files[0];
            pdffile_url=URL.createObjectURL(pdffile);
            $('#viewer').attr('src',pdffile_url);
            $('#viewer').show();
        }else{
            //$('#viewer').hide();
                      document.getElementById('master_file').value = ''

                swal(
                            'Error!',
                            'File Size Exceeds !!!',
                             'error'
                          );

                return false;
        }
      }else{
        document.getElementById('master_file').value = ''
          swal(
                'Error!',
                'Invalid File ! Please select a PDF file to upload',
                'error'
              );
         return false;
       
      }
      /* if(valid_extensions.test(filename)){ 

             pdffile=document.getElementById("master_file").files[0];
             pdffile_url=URL.createObjectURL(pdffile);
             $('#viewer').attr('src',pdffile_url);
           }else{
             alert('Invalid File ! Please select a PDF file to upload');
             return false;
        }*/
      });

    }

    function uploadFormValidate(){
          
          $('#master_question_form').validate({

            rules : {
                exam : {
                required : true,    
            },
           /* verifyPdf : {
              required : true,  
            },*/
            verifyImages: {
              required : true,  
            },

      },
      messages : {
        exam : {
          required : "<font color='red'>Required</font>",
        },
        /* verifyPdf : {
          required : "<font color='red'>Required</font>",
        },*/
        verifyImages: {
          required : "<font color='red'>Required</font>",
        } 
        
      },
      submitHandler: function(form) {
         $('.request-overlay').show();
          return true;        }
    });

    }
}
if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Master_question(config);
