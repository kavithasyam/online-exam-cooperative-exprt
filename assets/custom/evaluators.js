/*
 * File : Common script file for camp functions
 * @author : Jyothi
*/

function Evaluators(config){

    var $doc = $(document);
    
	var config = (typeof(config) == 'undefined') ? {} : config;
	if(config.init == 1)
	{
		init();
	}
     
     function init()
	{
		$doc.ready(function(){
         
         initSelect2();
         showCenters();
         showCheifEvaluators();
         //checkAdCheifEvaluator();
         SubmitEvaluators();
         checkAdCheifEvaluator();
         checkEvaluators();
         showDeleteConfirm();         
        
			
     


     $.validator.addMethod("alpha", function(value, element) {
          return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
      },"Only Characters Allowed.");

      $.validator.addMethod("alphanumeric", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9 &\-)( ]+$/i.test(value);
      }, "Only Characters and Numbers Allowed");

      $.validator.addMethod("mobileno", function(value,element) {
                    return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
      }, "Unsupported Format");
			
		});
	}

  
function initSelect2(){
  
  
  $(".cl_select2").select2({
   // tags: true,
  });
}

function showCenters(){

  $doc.on('change','#camp_name',function(){
    $('#subpage').html('');
     var campID = $(this).val();
     $.ajax({
        url:base_url+"index.php/Evaluators/getCentersAjax/",
        type:'POST',
        data: {caid : campID },
        dataType : 'html',
        success:function(response){
            
             $('#center').html(response);
            if(response.status == "success"){
                details = response.details;
                 
            }
        }
      });
  });
}


function showCheifEvaluators(){

  $doc.on('change','#center',function(){
    $('#subpage').html('');
     $('#replace_chk').html('');
     var campID = $('#camp_name').val();
     var cid = $(this).val();
     var newLevel = $('#newLevel').val();
     if(campID != "" && cid != ""){
       $.ajax({
          url:base_url+"index.php/Evaluators/getCheifEvaluatorsAjax/",
          type:'POST',
          data: {caid : campID, cid:cid,newlevel : newLevel },
          dataType : 'html',
          success:function(response){
              $('#subpage').html(response);
              initSelect2();
              checkEvaluators();
              SubmitEvaluators();
              showSelectedPanel();
              showExistingPanel();
              removeRow();

          }
        });
    }
  });
}

function removeRow(){

  $doc.on('click','.del_btn',function(){
    var $this = $(this);
    var table = $this.closest('table');
    var trcount = $('tr',table.find('tbody')).length;
    if(trcount >1){
      $this.closest('tr').remove();
      trcount = $('tr',table.find('tbody')).length;
      if(trcount ==1){
        table.find('.del_btn').hide();
      }
    }
   //$("#product_table tbody tr:first").clone();
            
    
  });
}

function checkAdCheifEvaluator(){
  
  $doc.on('change','.cl_ace_evaluators',function(){
    var $this = $(this);
     var campID = $('#camp_name').val();
     var ace_eval = $this.val();
     var ceEval = $this.parent().parent().find('.cl_ceEval').val();
     //console.log(ceEval);
      var selected = [];

      $('.cl_ace_evaluators').not(this).each(function(index, select){    
           if (select.value == ace_eval) { 
                  swal("Oops!",'You have already selected the Additional cheif evaluator', "error");
                    $this.val("");
                    return false;
            }

         });   
     
     $.ajax({
        url:base_url+"index.php/Evaluators/checkAdCheifEvaluatorAjax/",
        type:'POST',
        data: {caid :campID,ace_eval:ace_eval,ceEval:ceEval },
        dataType : 'json',
        success:function(response){
          
          if(response.status == "error"){
            swal("Oops!",response.msg , "error");
             $this.val("");
          }
        }
      });
  });

}

function checkEvaluators(){
  
  $doc.on('change','.cl_evaluators',function(){
    var $this = $(this);
     var campID = $('#camp_name').val();
     var eval = $this.find('option:selected');
      var aceEval = $this.parent().parent().find('.cl_ace_evaluators').val();
     var selected = [];
      eval.each( function( j, toption ) {
          selected.push(toption.value);
        });
      
      
    /*  $('.cl_evaluators').not(this).each(function(index, select){

        var $options = $(select).find('option:selected');
         $options.each( function( i, option ) {
             var index = $.inArray( option.value, selected );
                  if( index != -1 ) {
                     swal("Oops!",'You have already selected the evaluator', "error");
                    $this.parent().find('.select2-selection__rendered').find('li[title="'+option.text+'"]').remove();
                    $this.find('option[value="'+option.value+'"]').prop("selected", false);
                  }
        });

     });   */


     
  });

}

function SubmitEvaluators(){
   $('#evaluator_form').validate({
      rules : {
        
        camp              : { required : true },
        center            : { required : true },
		//newExam_panel        : { required:".check_samePanel:checked"}
      
      },
      messages : {
        
        camp            : { required : "<font color='red'>Required</font>"},
        center          : { required : "<font color='red'>Required</font>"},
		//newExam_panel   : { required : "<font color='red'>Required</font>"},
      },
       submitHandler: function(form) {
		   
       var haserror = 1;
       evaluators = $('.cl_evaluators');
       evaluators.each(function () {
          var value = $(this).val();
          if(value != null ){
             haserror = 0;
          }
        });
		
        if($(".check_samePanel").is(':checked')){
 
             var e = document.getElementById("exam_panel");
             if(e.options[e.selectedIndex].value==""){
				  swal("error!",'Select Previous Panel' , "error");
				
				  //$('#error_div').html('<font color="red" size="6"></font>');
				   return false;
			 }                           
        }  
		
         if(haserror != 0){
          $('#error_div').html('<font color="red" size="6"> Evaluators Required</font>');
          return false;
        }else{
          $('#error_div').html('');
         return true;
       }
     }

      });
}

  
  function showDeleteConfirm(){

    $doc.on('click','.del_evals',function(evt){
       evt.preventDefault();
       var id = $(this).data('id');
      
        $.confirm({
          'title'   : 'Delete Confirmation',
          'message' : 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
          'buttons' : {
          'Yes' : {
            'class' : 'blue',
            'action': function(){
                window.location.href = base_url+"index.php/Evaluators/deleteEvaluators/"+id;
              }
          },
          'No'  : {
            'class' : 'gray',
            'action': function(){
               return false;
              }  // Nothing to do in this case. You can as well omit the action property.
            }
          }
        });
    });
  }

  /*****************************New functions after psc mock evaluation***************************/

  function showExistingPanel(){
    $('.check_samePanel').on('change',function(){

       if(this.checked){
         $('.divExam').show();
		 $('.evalTable').remove();
		 // $('#subpage').html('');
       }else{
        $('.divExam').hide();
		$('#center').trigger('change');
       }

    });
    
  }
  function showSelectedPanel(){

    $('#exam_panel').on('change',function(){
         if($('#exam_panel').val() != ""){
         // $('.check_level').not(this).prop('checked', false);
          var $level = $('#exam_panel').val();
          var $newlevel = $('#newLevel').val();

           $('.chkbox_div').appendTo('#replace_chk');
          $('#subpage').html('');
          var campID = $('#camp_name').val();
          var cid = $('#center').val();
          if(campID != "" && cid != ""){           
             $.ajax({
                url:base_url+"index.php/Evaluators/getSelectedPanel/",
                type:'POST',
                data: {caid : campID, cid:cid,level:$level,newlevel :$newlevel },
                dataType : 'html',
                success:function(response){

                    $('#subpage').html(response);
                    initSelect2();
                    checkEvaluators();
                    SubmitEvaluators();
                    showSelectedPanel();
                }
              });
          }

        }
    });

     $('.check_level').on('change',function(){
         if($(this).val() != ""){
         // $('.check_level').not(this).prop('checked', false);
          var $level = $(this).val()
          var $newlevel = $('#newLevel').val();

           $('.chkbox_div').appendTo('#replace_chk');
          $('#subpage').html('');
          var campID = $('#camp_name').val();
          var cid = $('#center').val();
          if(campID != "" && cid != ""){           
             $.ajax({
                url:base_url+"index.php/Evaluators/getSelectedPanel/",
                type:'POST',
                data: {caid : campID, cid:cid,level:$level,newlevel :$newlevel },
                dataType : 'html',
                success:function(response){

                    $('#subpage').html(response);
                    initSelect2();
                    checkEvaluators();
                    SubmitEvaluators();
                    showSelectedPanel();
                }
              });
          }

        }
    });


  }

}
if(typeof(objmode) == 'undefined')
	{ 
		var config = {init:1};
	}
else{ var config = {init:0};	}	 
var objmode = new Evaluators(config);
